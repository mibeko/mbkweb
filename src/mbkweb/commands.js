import path from 'path';
import shell from 'shelljs';
import { lookpath } from 'lookpath';
import logger from './logger';

const tools = [
  'git',
  'gh',
  'lando',
  'docker',
];

shell.config.verbose = true;

export var checkTools = async () => {
  let tool;
  for (var i = 0; i < tools.length; i++) {
    tool = await lookpath(tools[i]);
    if (tool === "undefined" || tool === undefined) {
      logger.error(`CLI Tool ${tools[i]} not found. ${tool}...`);
      exit(1);
    } else {
      logger.info(`${tools[i]} found.`);
    }
  }
  logger.info(`All tools found!`);
}

export var gitClone = config => {
  logger.info(`Cloning website boilerplate`);
  shell.cd(config.files.root);
  shell.exec(`git clone ${config.repo} ${config.slug}`);
  shell.cd(config.slug);
  shell.rm(`-rf`, `.git`);
  shell.exec(`git init`);
  logger.info(`Adding remote git@github.com:mibeko-web/${config.slug}.git`);
  shell.exec(`gh repo create mibeko-web/${config.slug} --private -y`);
  shell.exec(`git remote add origin git@github.com:mibeko-web/${config.slug}.git`);
  shell.exec(`git branch -M master`);
  shell.exec(`git add -A`);
  shell.exec(`git commit -m 'init'`);
}

export var installPackages = config => {

  // composer
  shell.cd(path.join(config.files.root, config.slug));
  shell.exec(`composer install`);
  shell.exec(`composer install --working-dir=web/app/mu-plugins/mibeko-plug`);
  shell.exec(`composer install --working-dir=web/app/themes/${config.slug}`);

  // yarn
  shell.exec(`yarn --cwd web/app/themes/${config.slug}`)
}

export var landoStart = config => {
  shell.cd(path.join(config.files.root, config.slug));
  logger.info(`Starting Lando`);
  shell.exec(`lando start`);
  logger.info(`Importing database`);
  shell.exec(`lando db-import db.sql.gz`);
  logger.info(`Performing search-replacements to database`);
  var getPrefixQueries = shell.exec(
    `lando mysql -B -sN -e 'SELECT concat("RENAME TABLE ", TABLE_NAME, " TO ", replace(TABLE_NAME, "mbkwb_", "${config.dbPrefix}"), ";") FROM information_schema.TABLES WHERE TABLE_SCHEMA = "wordpress";'`,
    (code, stdout, stderr) => {

      // execute prefix queries
      var output = stdout.toString();
      shell.exec(`lando mysql wordpress -e '${output}'`);

      // make extra prefix replacements
      shell.exec(`lando mysql wordpress -e 'UPDATE \`${config.dbPrefix}usermeta\` SET meta_key = REPLACE(meta_key, "mbkwb_", "${config.dbPrefix}") WHERE meta_key LIKE "mbkwb_%";'`);
      shell.exec(`lando mysql wordpress -e 'UPDATE \`${config.dbPrefix}options\` SET option_value = replace(option_value, "mbkwb_", "${config.dbPrefix}") WHERE option_name LIKE "mbkwb_%";'`);

      logger.info(`Replacing siteslug`);
      shell.exec(`lando wp search-replace 'mbkweb' '${config.slug}'`);

      logger.info(`Replacing SiteName`);
      shell.exec(`lando wp search-replace 'MBKW' '${config.name}'`);
    }
  );
}
