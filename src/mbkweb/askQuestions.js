import inquirer from 'inquirer';

const questions = [
  {
    type: 'input',
    name: 'name',
    message: 'Site name?',
    default: 'Mibeko Web',
  },
  {
    type: 'input',
    name: 'slug',
    message: 'Site slug?',
    default: 'mbkweb',
  },
  {
    type: 'input',
    name: 'productionUrl',
    message: 'Site production URL?',
    default: 'mbkproductions.io',
  },
  {
    type: 'input',
    name: 'dbName',
    message: 'Database name?',
    default: 'mbkweb',
  },
  {
    type: 'input',
    name: 'dbPrefix',
    message: 'Database prefix?',
    default: 'mbkwb_',
    filter: async val => {
      if (await !val.endsWith('_')) val += '_';
      return val;
    },
  }
];

export default async () => {
  return await inquirer.prompt(questions);
}
