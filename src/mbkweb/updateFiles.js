import fs from 'fs';
import path from 'path';
import replace from 'replace-in-file';
import generateSalt from './generateSalt';
import logger from './logger';

var sitePath = (config, ...paths) => {
  return path.join(config.files.root, config.slug, ...paths);
}

var generateSalts = keys => {
  var salts = '';
  for (var j = 0; j < keys.length; j++) {
    salts += keys[j] + '=\'' + generateSalt() + '\'';
    if (j < keys.length - 1) {
      salts += '\n';
    }
  }
  return salts;
}

export default async config => {
  logger.info('Updating site variables');
  logger.info('{{siteslug}}');
  await replace({
    files: [
      sitePath(config, config.files.lando),
      sitePath(config, config.files.env.development),
      sitePath(config, config.files.env.staging),
      sitePath(config, config.files.cap.deploy),
      sitePath(config, config.files.theme.webpack),
      sitePath(config, config.files.theme.stylesheet),
      sitePath(config, config.files.theme.dir, 'app/**/*.php'),
      sitePath(config, config.files.theme.dir, 'bootstrap/app.php'),
      sitePath(config, config.files.theme.dir, 'resources/views/**/*.php'),
    ],
    from: /{{siteslug}}/g,
    to: config.slug,
  });

  logger.info('{{sitename}}');
  await replace({
    files: [
      sitePath(config, config.files.theme.stylesheet),
    ],
    from: /{{sitename}}/g,
    to: config.name,
  });

  logger.info('{{siteurl}}');
  await replace({
    files: [
      sitePath(config, config.files.env.production),
      sitePath(config, config.files.cap.production),
    ],
    from: /{{siteurl}}/g,
    to: config.productionUrl,
  });

  logger.info('{{dbname}}');
  await replace({
    files:[
      sitePath(config, config.files.env.staging),
      sitePath(config, config.files.env.production),
    ],
    from: /{{dbname}}/g,
    to: config.dbName,
  });

  logger.info('wp_');
  if (config.dbPrefix !== 'wp_') {
    await replace({
      files: [
        sitePath(config, config.files.application),
      ],
      from: /wp_/g,
      to: config.dbPrefix,
    });
  }

  logger.info('{{firstletter}}');
  await replace({
    files: [
      sitePath(config, config.files.env.staging),
      sitePath(config, config.files.env.production),
    ],
    from: /{{firstletter}}/g,
    to: config.slug.charAt(0).toUpperCase(),
  });

  logger.info('{{salts}}');
  await replace({
    files: sitePath(config, config.files.env.development),
    from: /{{salts}}/g,
    to: generateSalts(config.keys),
  });
  await replace({
    files: sitePath(config, config.files.env.staging),
    from: /{{salts}}/g,
    to: generateSalts(config.keys),
  });
  await replace({
    files: sitePath(config, config.files.env.production),
    from: /{{salts}}/g,
    to: generateSalts(config.keys),
  });

  logger.info('cp .env.development .env');
  await fs.copyFile(
    sitePath(config, config.files.env.development),
    sitePath(config, '.env'),
    err => {
      if (err) throw err;
    }
  );

  // site_name (theme dir name)
  logger.info('mv site_name ' + config.slug);
  var oldThemeDir = sitePath(config, config.files.theme.dir);
  var newThemeDir = sitePath(config, config.files.theme.dir, '..', config.slug);
  fs.rename(
    oldThemeDir,
    newThemeDir,
    err => {
      if (err) throw err;
    }
  );
}
