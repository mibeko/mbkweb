import winston from 'winston';

const format = winston.format.combine(
  winston.format.colorize(),
  winston.format.align(),
  winston.format.printf(info => `${info.level}: ${info.message}`),
);

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({format}),
  ],
});

export default logger;
