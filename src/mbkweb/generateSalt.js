import getRandomValues from 'get-random-values';

const minChar = 33; // !
const maxChar = 126; // ~

// (c) Austin Pray 2016
// MIT License

// https://github.com/EFForg/OpenWireless/blob/0e0bd06277f7178f840c36a9b799c7659870fa57/app/js/diceware.js#L59
var getRandom = (min, max) => {
  var rval = 0;
  var range = max - min;

  var bits_needed = Math.ceil(Math.log2(range));
  if (bits_needed > 53) {
    throw new Exception("We cannot generate numbers larger than 53 bits.");
  }
  var bytes_needed = Math.ceil(bits_needed / 8);
  var mask = Math.pow(2, bits_needed) - 1; 
  // 7776 -> (2^13 = 8192) -1 == 8191 or 0x00001111 11111111

  // Create byte array and fill with N random numbers
  var byteArray = new Uint8Array(bytes_needed);
  getRandomValues(byteArray);

  var p = (bytes_needed - 1) * 8;
  for(var i = 0; i < bytes_needed; i++ ) {
    rval += byteArray[i] * Math.pow(2, p);
    p -= 8;   
  }

  // Use & to apply the mask and reduce the number of recursive lookups
  rval = rval & mask;

  if (rval >= range) {
    // Integer out of acceptable range
    return getRandom(min, max);
  }
  // Return an integer that falls within the range
  return min + rval;
}

var getRandomChar = () => {
  var char = String.fromCharCode(getRandom(minChar, maxChar));
  if (["'", "\"", "\\"].some(function (badChar) { return char === badChar})) {
    return getRandomChar();
  }

  return char;
}

export default () =>  {
  return Array.apply(null, Array(64)).map(getRandomChar).join("");
}
