import 'regenerator-runtime/runtime';
import 'core-js/stable';
import merge from 'lodash.merge';
import * as cmd from './mbkweb/commands';
import askQuestions from './mbkweb/askQuestions';
import updateFiles from './mbkweb/updateFiles';
import config from './config';

export default async () => {
  await cmd.checkTools();
  const answers = await askQuestions();
  merge(config, answers);
  cmd.gitClone(config);
  await updateFiles(config);
  cmd.installPackages(config);
  cmd.landoStart(config);
}
